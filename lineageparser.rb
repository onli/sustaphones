require 'yaml'
require 'yaml/store'
require 'date'

# Parse the lineage wiki's yaml and write into the database which phones are supported

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesdir = File.join(basedir, 'lineage_wiki', '_data', 'devices')
# Keeps all the ids we see, so that we can remove orphans in our db afterwards
lineage_devices = []

db.transaction do
    Dir.children(devicesdir).each do |devicefile|
        devicedata = YAML.load_file(File.join(devicesdir, devicefile),  permitted_classes: [Date])

        puts devicedata['codename']
        id = devicedata['codename']
        lineage_devices.push(id)

        db[id] = {} if db[id].nil?
        next if db[id]['dummy']
        vendor = devicedata['vendor']
        vendor = 'Google' if vendor == 'pixel' # corrects source data error permanently
        db[id]['vendor'] = vendor unless db[id]['vendor']
        db[id]['name'] = devicedata['name'] unless db[id]['name']
        db[id]['release'] = devicedata['release'] unless db[id]['release']
        db[id]['type'] = devicedata['type'] unless db[id]['type']
        db[id]['battery'] = devicedata['battery'] unless db[id]['battery']
        db[id]['sdcard'] = devicedata['sdcard'] unless db[id]['sdcard']
        if devicedata['maintainers'].count > 0
            db[id]['lineage_version'] = devicedata['current_branch']
        else
            # devices without maintainers are abandoned, and this is the main method how devices were
            # removed after the LOS 21 update
            db[id].delete('lineage_version') if db[id] && db[id]['lineage_version']
        end
        db[id]['unlockable'] = (devicedata['is_unlockable'].nil? ? true : devicedata['is_unlockable'])
        db[id]['volte_locked'] = devicedata['quirks'] && devicedata['quirks'].include?('ims')
        unless db[id]['headphone_jack']
            if devicedata['peripherals'].include?('3.5mm jack')
                db[id]['headphone_jack'] = 'Yes'
            else
                db[id]['headphone_jack'] = 'No'
            end
        end
    end
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])
db.transaction do
    devices.each do |id, _|
        if ! lineage_devices.include?(id)
            # The calyx-wiki has no entry for this device, so if we stored it as supported
            # we have to delete that entry now
            db[id].delete('lineage_version')
        end
    end
end
