module.exports = {
  content: ['html/index.html'],
  css: ['html/css/bulma.min.css'],
  safelist: {
          standard: ["html", "body", "dropdown-menu", "is-active", "is-dark" ], 
          deep: [/^col/, /^navbar/, /^nav/]
      },
  output: 'html/css/stripped/'
}