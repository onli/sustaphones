require 'yaml'
require 'yaml/store'
require 'date'
require 'json'
require 'httparty'
require 'addressable/uri'
require 'oga'

# TODO: Replace the website parsing with a parser for the files in https://gitlab.com/iode/ota

db = YAML::Store.new('devices.yaml')

def download(url)
    puts 'download'
    url = Addressable::URI.parse(url).normalize.to_s
    begin
        response = HTTParty.get(url, {headers: {"User-Agent" => "Sustaphone/1.0"}})
        data = response.body
        return data
    rescue => e
        warn r
    end
    return nil
end

def extract_codename(el)
    puts 'extracting codename'
    begin
        ex = el.rpartition(' ').last
        re = /\((?<codename>.+)\)/
        dev = ex.match(re)
        return dev[:codename]
    end
end

def xpath2version(db, devices, path)
    puts 'checking versions with xpath'
    db.transaction do
        begin
            devices.xpath(path).each do |el|
                el.xpath('tr').each do |tr|
                    device = tr.xpath('td[2]').text
                    p device
                    next if !device.match(/\(.*\)/)
                    id = extract_codename(device)
                    # if device id not already present, skip
                    next if db[id].nil?
                    puts id
                    db[id]['iode_version'] = tr.xpath('td[3]').text.to_i
                    db[id]['iode_installer'] = tr.xpath('td[4]').text
                end
            end
        rescue => e
            warn r
        end
    end
end

iode_post = 'https://iode.tech/iodeos-official-supported-devices/'
iode_html = download(iode_post)

devices = Oga.parse_html(iode_html)

xpath_version = '//*[@id="tablepress-1"]/tbody'

xpath2version(db, devices, xpath_version)
