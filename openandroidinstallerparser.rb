require 'yaml'
require 'yaml/store'
require 'date'

db = YAML::Store.new("devices.yaml")
devices = YAML.load_file('devices.yaml', permitted_classes: [Date])

basedir = File.expand_path(File.dirname(__FILE__))
installerdir = File.join(basedir, 'openandroidinstaller', 'openandroidinstaller', 'assets', 'configs')

db.transaction do
    devices.each do |id, values|
        next if values['dummy']
        puts id
        begin
            if File.exist?(File.join(installerdir, id + '.yaml'))
                puts 'set installer support'
                db[id]['openandroidinstaller'] = 'Yes'
            else
                puts 'unset installer support'
                db[id]['openandroidinstaller'] = 'No'
            end
        rescue => e
            warn e
        end
    end
end