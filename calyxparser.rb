# Parse the calyxos devices yaml file for the website

# https://gitlab.com/CalyxOS/calyxos.org

require 'yaml'
require 'yaml/store'
require 'date'

db = YAML::Store.new('devices.yaml')

basedir = File.expand_path(File.dirname(__FILE__))
devicesfile = File.join(basedir, 'calyxos.org', 'pages', '_data', 'devices.yml')
calyx_devices = YAML.load_file(devicesfile, permitted_classes: [Date])

db.transaction do
    calyx_devices.each do |id, devicedata|
        next unless devicedata.is_a?(Hash)
        
        puts id
        db[id] = {} if db[id].nil?
        vendor = devicedata['brand']
        vendor = 'Google' if vendor == 'pixel'
        db[id]['vendor'] = vendor unless db[id]['vendor']
        db[id]['name'] = devicedata['model'] unless db[id]['name']
        puts 'add release year manually!' unless db[id]['release']
        puts 'add battery data manually' unless db[id]['battery']

        db[id]['calyxos_version'] = devicedata['android']
    end
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])
db.transaction do
    devices.each do |id, devicedata|
        if ! calyx_devices.has_key?(id) || calyx_devices['previous_codenames'].include?(id)
            # The calyx-wiki has no entry for this device, so if we stored it as supported
            # we have to delete that entry now
            db[id].delete('calyxos_version')
        end
    end
end