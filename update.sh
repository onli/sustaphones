#!/bin/sh
cd lineage_wiki/
git pull
cd ../e_docs
git pull
cd ../calyxos.org/
git pull
cd ../axpos-org/
git pull
cd ../openandroidinstaller/
git pull
cd ..
bundle exec ruby lineageparser.rb
notify-send "lineage update done"
bundle exec ruby androideparser.rb
notify-send "/e/ update done"
bundle exec ruby calyxparser.rb
notify-send "calyx update done"
bundle exec ruby iodeparser.rb
notify-send "iodé update done"
bundle exec ruby axposparser.rb
notify-send "axpos update done"
bundle exec ruby openandroidinstallerparser.rb
notify-send "installer update done"
bundle exec ruby ifixitparser.rb
notify-send "ifixit update done"

notify-send "check devices and run pagebuilder"
