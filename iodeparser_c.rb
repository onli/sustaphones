require 'yaml'
require 'yaml/store'
require 'date'
require 'json'
require 'httparty'
require 'addressable/uri'
require 'oga'

db = YAML::Store.new('devices.yaml')

def download(url)
    url = Addressable::URI.parse(url).normalize.to_s
    begin
        response = HTTParty.get(url, {headers: {"User-Agent" => "Sustaphone/1.0"}})
        data = response.body
        return data
    end
    return nil
end

def xpath2version(db, devices, path)
  begin
    devices.xpath(path).each do |el|
      el.xpath('tr').each do |tr|
        id = tr.xpath('td[3]').text
        next if id.empty?
        db.transaction do
          # if device id not already present, skip
          next if db[id].nil?
          puts id
          iode_version = tr.xpath('td[5]').text
          # offers atm both, iodé version or Android - use latter
          db[id]['iode_version'] = iode_version.rpartition(' ').last.to_i
        end
      end
    end
  end
end

forum_post = 'https://community.iode.tech/t/editable-list-iodeos-official-unofficial-devices/1877'
forum_post_json = forum_post + '.json'

json = JSON.parse(download(forum_post_json))
post = json['post_stream']['posts'][0]['cooked']

devices = Oga.parse_html(post)

xpath_official = 'div[1]/table/tbody'
xpath_unofficial = 'div[2]/table/tbody'

xpath2version(db, devices, xpath_official)
xpath2version(db, devices, xpath_unofficial)
