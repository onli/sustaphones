require 'yaml'
require 'yaml/store'
require 'date'
require 'oga'

db = YAML::Store.new('devices.yaml')

basedir = File.expand_path(File.dirname(__FILE__))
devicesfile = File.join(basedir, 'grapheneos.org', 'static', 'faq.html')
devices = Oga.parse_html(open(devicesfile))

# supported devices are in 3 groups. eol=endoflife as in no manufacturer firmware updates
#   android14-main (p5a - p8*)
#   android14-main-eol (p5/redfin, p4a5g/bramble)
#   android13-legacy-eol (p4a/sunfish, p4xl/coral, p4/flame)

csspath = 'main#faq section#device-support article#supported-devices ul li'
xpath = '//*[@id="supported-devices"]/ul/li'
xpath_main = '//*[@id="supported-devices"]/ul[1]/li'
xpath_main_eol = '//*[@id="supported-devices"]/ul[2]/li'
xpath_legacy_eol = '//*[@id="supported-devices"]/ul[3]/li'

def extract(el)
  begin
    ex = el.text.rpartition(' ').last
    re = /\((?<codename>.+)\)/
    dev = ex.match(re)
    return dev[:codename]
  end
end

def xpath2version(db, devices, path, version)
  begin
    devices.xpath(path).each do |el|
      id = extract(el)
      db.transaction do
        next if db[id].nil?
        puts id
        db[id]['grapheneos_version'] = version
      end
    end
  end
end

xpath2version(db, devices, xpath_main, '14')
xpath2version(db, devices, xpath_main_eol, '14-eol')
xpath2version(db, devices, xpath_legacy_eol, '13-eol')
