# Parse the /e/ wiki sources and write into the database which phones are supported

# https://gitlab.e.foundation/e/documentation/user

require 'yaml'
require 'yaml/store'
require 'date'

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesdir = File.join(basedir, 'e_docs', 'htdocs' ,'_data', 'devices')
# Keeps all the ids we see, so that we can remove orphans in our db afterwards
e_devices = []

db.transaction do
    Dir.children(devicesdir).each do |devicefile|
        devicedata = YAML.load_file(File.join(devicesdir, devicefile), permitted_classes: [Date])

        puts devicedata['codename']
        id = devicedata['codename']
        e_devices.push(id)
        
        db[id] = {} if db[id].nil?
        vendor = devicedata['vendor']
        vendor = 'Google' if vendor == 'pixel' # corrects source data error permanently
        db[id]['vendor'] = vendor unless db[id]['vendor']
        db[id]['name'] = devicedata['name'] unless db[id]['name']
        db[id]['release'] = devicedata['release'] unless db[id]['release']
        db[id]['type'] = devicedata['type'] unless db[id]['type']
        db[id]['battery'] = devicedata['battery'] unless db[id]['battery']
        db[id]['sdcard'] = devicedata['sdcard'] unless db[id]['sdcard']
        if devicedata['legacy'] == 'yes'
            # Legacy devices are listed, but don't have install files available
            db[id].delete('e_version') if db[id] && db[id]['e_version']
        else
            if devicedata['build_version_stable']
                e_version = devicedata['build_version_stable'].respond_to?('any?') ? devicedata['build_version_stable'].first : devicedata['build_version_stable']
            else
                e_version = devicedata['build_version_dev'].respond_to?('any?') ? devicedata['build_version_dev'].first : devicedata['build_version_dev']
            end
            e_version = 'q' if e_version.to_s == '17.1'
            e_version = e_version.downcase
            db[id]['e_version'] = e_version
        end
        devicedata['install'].each_with_object({}) do |i|
            mode = i.values_at('mode')
            if mode.include?('Easy Installer')
                db[id]['easy_installer'] = 'Yes'
            end
        end
    end
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])
db.transaction do
    devices.each do |id, _|
        if ! e_devices.include?(id)
            # The calyx-wiki has no entry for this device, so if we stored it as supported
            # we have to delete that entry now
            db[id].delete('e_version')
        end
    end
end