var options = {
  valueNames: [ 'devicename', 'vendor', 'lineage_version', 'e_version', 'calyxos_version', 'iode_version', 'axpos_version', 'release', 'battery_repairscore', 'headphone_jack', 'sdcard', 'openandroidinstaller']
};

// The set of filters, given by the user
var filters = {};
// The list.js filter object
var listObj = null;
// True when user had select pressed on click. Used for not closing the dropdown when user seem to try to change multiple options via ctrl-click
var ctrlMode = false;
// True when the body click event handler is done. Used to wait for it, as only in the click handler can see whether ctrl was pressed
var clickEvaluated = false;

// Let list.js start filtering the DOM. Afterwards we can change that filter with listObj.search and .filter
// This is its own function to boost initial page render time
function init() {
    if (listObj === null) {
        listObj = new List('androids', options);
    }
}

// Get all parameters an URLSearchParams object
function getParams() {
    return new URLSearchParams(window.location.search);
}

// Set GET parameters by replacing history state, deletes if set to ''
function setOrUpdateParameter(key, value) {
    var dummy = new URL(location.href);
    dummy.searchParams.set(key, value); 
    if (value == '') {
        dummy.searchParams.delete(key);
    }
    history.replaceState(null, '', dummy.href)
}               

// Apply filters as stored in URL fragment
var params = getParams()
if (params.size > 0) {
    init();
    if (params.get('s')) {
        listObj.search(params.get('s'));
        document.querySelector('#search-field').value = decodeURIComponent(params.get('s'));
    }
    for (const element of params.keys()) {
        if (element != 's') {
            try {
                filters[element] = new Set(JSON.parse(decodeURIComponent(params.get(element))));
            } catch (SyntaxError) {
                continue;
            }
            document.querySelector('#select' + element).closest('.dropdown').querySelector('button').classList.add('is-dark');
            filters[element].forEach(function (item) {
                var options = document.querySelector('#select' + element).options
                for (let i=0;i<options.length;i++) {
                    if (options[i].innerHTML == item) {
                        options[i].selected = true
                    }
                }
            });
        }
    }
    applyFilters();
}

// This does the actual filtering, for all configured filters
function applyFilters() {
    listObj.filter(function(item) {
        for (var i=0; i < Object.keys(filters).length; i++) {
            if (! Object.values(filters)[i].has(item.values()[Object.keys(filters)[i]].trim())) {
                return false;
            }
        }
        return true;
    });
}


var searchTimeout = null;
document.querySelector('#search-field').addEventListener('keyup', function(e) {
    init();
    var searchString = this.value;
    setOrUpdateParameter('s', searchString);
    clearTimeout(searchTimeout);
    searchTimeout = setTimeout(() => listObj.search(searchString), 200);
});

document.querySelectorAll('select').forEach(function(select) {
    select.addEventListener('change', async function(e) {
        clickEvaluated = false;
        init();
        if (e.target.selectedOptions.length == 0 || (e.target.selectedOptions.length == 1 && e.target.selectedIndex === 0)) {
            // The dropown was selected, but got unselected or 'all' was clicked. Now we can delete this filter
            delete filters[select.dataset.target]
            setOrUpdateParameter(select.dataset.target, '');
            e.target.closest('.dropdown').querySelector('button').classList.remove('is-dark');
        } else {
            // The dropown was selected. Now we can add this filter
            // Note the use of a Set that takes all selected dropdown options, for multiselect
            filters[select.dataset.target] = new Set(Array.from(e.target.selectedOptions).map(({ value }) => value));
            setOrUpdateParameter(select.dataset.target, JSON.stringify(
                  filters[select.dataset.target],
                  // Note how the Set has to be treated, without this replacer stringify would fail
                  (_key, value) => (value instanceof Set ? [...value] : value) 
                )
            );
            e.target.closest('.dropdown').querySelector('button').classList.add('is-dark');
        }

        applyFilters();

        while (! clickEvaluated) {
            // We need to wait for the click handler it. It runs after the change handler or just takes longer.
            // Only afterward do we know whether ctrl was pressed
            await new Promise(r => setTimeout(r, 10));
        }
        if (! ctrlMode) {
            // If we don't close the dropdown now, the dropdown UI behaviour feels strange
            e.target.closest('.dropdown').classList.remove('is-active');
        }
    });
});

// Dropdown UI behaviour
document.querySelector('body').addEventListener('click', function(e) {
    ctrlMode = e.ctrlKey;
    
    // Close all drowpdowns when clicking anywhere outside of a dropdown
    if (e.target.closest('.dropdown') == null && ! e.target.closest('.navbar-burger')) {
        document.querySelectorAll('.is-active').forEach(function(dropdown) {
            dropdown.classList.toggle('is-active');
        });
    }
    
    if (e.target.tagName == 'BUTTON' || e.target.closest('button')) {
        var wasOpen = e.target.closest('.dropdown').classList.contains('is-active');
        
        // When clicking on a button or on an option in a dropdown, we need to close all other dropdowns
        document.querySelectorAll('.is-active').forEach(function(dropdown) {
            dropdown.classList.toggle('is-active');
        });
        
        // If the dropdown was originally closed, we want to opepn it now. Otherwise we need to do nothing,
        // as we just closed all dropdowns
        if (! wasOpen) {
            e.target.closest('.dropdown').classList.add('is-active');
        }
    }
    clickEvaluated = true;
});


// Mobile burger navigation menu functionality:
// Get all "navbar-burger" elements
const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

// Add a click event on each of them
$navbarBurgers.forEach( el => {
    el.addEventListener('click', (e) => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
    });
});
