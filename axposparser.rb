require 'yaml'
require 'yaml/store'
require 'date'

# Parse the axpos params files and write into the database which phones are supported

db = YAML::Store.new("devices.yaml")

basedir = File.expand_path(File.dirname(__FILE__))
devicesdir = File.join(basedir, 'axpos-org', 'params')

axpos_devices = []

db.transaction do
    Dir.children(devicesdir).each do |devicefile|
        filedata = File.read(File.join(devicesdir, devicefile))
        begin
            id = /export HUGO_CODENAME="(.*)"/.match(filedata)[1]
            version = /export HUGO_ANDROID="(.*)"/.match(filedata)[1]
            state = /export HUGO_STATE="(.*)"/.match(filedata)[1]
        rescue NoMethodError => nme
            # Skips the empty INIT file
            warn "No id or version grabbed for #{devicefile}"
        end
        if (id && id != '' && state == 'stable') 
            db[id]['axpos_version'] = version
            axpos_devices.push(id)
        end
    end
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])

db.transaction do
    devices.each do |id, _|
        if ! axpos_devices.include?(id)
            # The axpos files have no entry for this device, so if we stored it as supported
            # we have to delete that entry now
            db[id].delete('axpos_version')
        end
    end
end