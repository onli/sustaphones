require 'yaml'
require 'erb'
require 'tilt'
require 'date'
require 'htmlcompressor'

# Take the information from the database about which phones are repairable and supported by Roms and create the html of the page

def translate_difficulty(difficulty, battery_removable)
    return 1 if battery_removable

    case difficulty
    when 'Very easy' then 1
    when 'Easy' then 2
    when 'Moderate' then 3
    when 'Difficult' then 4
    else 5 # Guides currently seemd not be rated more difficult than 'Difficult',
           # but repairing a device without a guide might still be more difficult
    end
end

def translate_lineage_version(lineage)
    return case lineage
    when 22.1 then 15.0
    when 21 then 14.0
    when 20 then 13.0
    when 19.1 then 12.0
    when 18.1 then 11.0
    when 17.1 then 10.0
    when 16.0 then 9.0
    when 15.1 then 8.1
    when 14.1 then 7.1
    when 13.0 then 6.0
    end
end

def translate_e_version(e)
    return case e
    when 'u' then 14.0
    when 't' then 13.0
    when 's' then 12.0
    when 'r' then 11.0
    when 'q' then 10.0
    when 'pie' then 9.0
    when 'oreo' then 8.1
    when 'nougat' then 7.1
    else 0
    end
end

def translate_iode_version(iode)
    return case iode
    when 6 then 15.0
    when 5 then 14.0
    when 4 then 13.0
    when 2 then 11.0
    end
end

def translate_versions(lineage, e, calyx, iode, axpos)
    [
        translate_lineage_version(lineage).to_f,
        translate_e_version(e).to_f,
        calyx.to_f,
        translate_iode_version(iode).to_f,
        axpos.to_f
    ].max
end

def release_to_year(release)
    release.respond_to?('any?') ? release.first.values.first.to_s.gsub(/-.+/, '')  : release.to_s.gsub(/-.+/, '')
end

# The main sort factor. Ideal if it is supported by one modern rom and the battery is easy to repair
def easy_and_modern(values)
    return 1 if (translate_versions(values['lineage_version'], values['e_version'], values['calyxos_version'], values['iode_version'], values['axpos_version']) >= 11.0) && translate_difficulty(values['battery_repairscore'], values['battery'].respond_to?('has_key?') ? values['battery']['removable'] : false) <= 2
    # if it is at least moderately easy to repair, and there are roms, that is still better than one of these conditions is false:
    return 2 if (translate_versions(values['lineage_version'], values['e_version'], values['calyxos_version'], values['iode_version'], values['axpos_version']) >= 11.0) && translate_difficulty(values['battery_repairscore'], values['battery'].respond_to?('has_key?') ? values['battery']['removable'] : false) == 3
    return 3    # the rest can be sorted by the old factors
end

devices = YAML.load_file('devices.yaml', permitted_classes: [Date])
# we want to filter out devices that have no known software support, to keep the page smaller
devices.keep_if{|id, values| values['lineage_version'] || values['e_version'] || values['calyxos_version'] || values['iode_version'] || values['axpos_version']}

###
# Phone section
###

phones = devices
            .select{|id, values| ! values['type'] || values['type'] == 'phone' }
            .sort_by{|id, values| [
                                        easy_and_modern(values),
                                        translate_difficulty(values['battery_repairscore'], values['battery'].respond_to?('has_key?') ? values['battery']['removable'] : false),
                                        - translate_versions(values['lineage_version'], values['e_version'], values['calyxos_version'], values['iode_version'], values['axpos_version']),
                                        ( - release_to_year(values['release']).to_i),
                                        values['headphone_jack'] == 'Yes' ? 0 : 1 ,
                                        values['sdcard'] ? 0 : 1,
                                        values['vendor']
                                        ]
                                        }

phone_filters = {}
phone_filters[:vendor] = phones.map{|id, x| x['vendor'] }.uniq.sort.reject{|x| x.nil? || x.empty? }
phone_filters[:release] = phones.map{|id, x| release_to_year(x['release']) }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:lineage_version] = phones.map{|id, x| x['lineage_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:e_version] = phones.map{|id, x| x['e_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:calyxos_version] = phones.map{|id, x| x['calyxos_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:iode_version] = phones.map{|id, x| x['iode_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:axpos_version] = phones.map{|id, x| x['axpos_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
phone_filters[:openandroidinstaller] = phones.map{|id, x| x['openandroidinstaller'].to_s }.uniq.reject{|x| x.nil? || x.empty? }
phone_filters[:battery_repairscore] = phones.map{|id, x| x['battery_repairscore'].to_s }.uniq.reject{|x| x.nil? || x.empty? }.sort_by{|x|
    case x
    when "Very easy" then 0
    when "Easy" then 1
    when "Moderate" then 2
    when "Difficult" then 3
    when "Very difficult" then 4
    end
}
phone_filters[:headphone_jack] = phones.map{|id, x| x['headphone_jack'].to_s }.uniq.reject{|x| x.nil? || x.empty? }
phone_filters[:sdcard] = phones.map{|id, x| x['sdcard'] ? 'Yes' : 'No' }.uniq.reject{|x| x.nil? || x.empty? }

###
# Tablet section
###

# Basically same code for now apart from the type check below. Could be generalized, but is likely
# to change over time

tablets = devices
            .select{|id, values| ! values['type'] || values['type'] == 'tablet'  }
            .sort_by{|id, values| [
                                        easy_and_modern(values),
                                        translate_difficulty(values['battery_repairscore'], values['battery'].respond_to?('has_key?') ? values['battery']['removable'] : false),
                                        - translate_versions(values['lineage_version'], values['e_version'], values['calyxos_version'], values['iode_version'], values['axpos_version']),
                                        ( - release_to_year(values['release']).to_i),
                                        values['headphone_jack'] == 'Yes' ? 0 : 1 ,
                                        values['sdcard'] ? 0 : 1,
                                        values['vendor']
                                        ]
                                        }

tablet_filters = {}
tablet_filters[:vendor] = tablets.map{|id, x| x['vendor'] }.uniq.sort.reject{|x| x.nil? || x.empty? }
tablet_filters[:release] = tablets.map{|id, x| release_to_year(x['release']) }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:lineage_version] = tablets.map{|id, x| x['lineage_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:e_version] = tablets.map{|id, x| x['e_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:calyxos_version] = tablets.map{|id, x| x['calyxos_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:iode_version] = tablets.map{|id, x| x['iode_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:axpos_version] = tablets.map{|id, x| x['axpos_version'].to_s }.uniq.sort.reject{|x| x.nil? || x.empty? }.reverse
tablet_filters[:openandroidinstaller] = tablets.map{|id, x| x['openandroidinstaller'].to_s }.uniq.reject{|x| x.nil? || x.empty? }
tablet_filters[:battery_repairscore] = tablets.map{|id, x| x['battery_repairscore'].to_s }.uniq.reject{|x| x.nil? || x.empty? }.sort_by{|x|
    case x
    when "Very easy" then 0
    when "Easy" then 1
    when "Moderate" then 2
    when "Difficult" then 3
    when "Very difficult" then 4
    end
}
tablet_filters[:headphone_jack] = tablets.map{|id, x| x['headphone_jack'].to_s }.uniq.reject{|x| x.nil? || x.empty? }
tablet_filters[:sdcard] = tablets.map{|id, x| x['sdcard'] ? 'Yes' : 'No' }.uniq.reject{|x| x.nil? || x.empty? }


basedir = File.expand_path(File.dirname(__FILE__))
view = File.join(basedir, 'views', 'index.erb')

layoutpath = File.join(basedir, 'views', 'layout.erb')
layout = Tilt::ERBTemplate.new(layoutpath)

context = Object.new    # this seems to be the only working way to make variables accessible in the layout
context.instance_variable_set(:@devices, phones)
context.instance_variable_set(:@filters, phone_filters)
context.instance_variable_set(:@page, :phones)

def context.android_lineage_version(version)
    translate_lineage_version(version)
end

def context.android_e_version(version)
    translate_e_version(version)
end

def context.android_iode_version(version)
    translate_iode_version(version)
end

# Return rating for android version. Follow this rule:
# The last two versions are fine, the one before still okay, everything else too old
# Change this as soon as the first ROM supports a new Android version
def context.android_version_rating(version)
    case version.to_f
        when 14..15 then "success"
        when 13 then "warning"
        else "danger"
    end
end

phone_output = layout.render(context) {
    # This construction ensures the layout is used
    template = Tilt::ERBTemplate.new(view)
    # The render of the template inside the layout has nothing to evaluate. But it needs the
    # the local collection, thus it gets an empty Object as first param
    template.render(context)
}

context.instance_variable_set(:@devices, tablets)
context.instance_variable_set(:@filters, tablet_filters)
context.instance_variable_set(:@page, :tablets)

tablet_output = layout.render(context) {
    template = Tilt::ERBTemplate.new(view)
    template.render(context)
}

targetpath = File.join(basedir, 'html', 'index.html')
File.write(targetpath, HtmlCompressor::Compressor.new.compress(phone_output))

targetpath = File.join(basedir, 'html', 'tablets.html')
File.write(targetpath, HtmlCompressor::Compressor.new.compress(tablet_output))

system( "npx purgecss -c purgecss.config.js" )
