# sustaphones

[![Netlify Status](https://api.netlify.com/api/v1/badges/3abac063-414c-4d97-a96f-1fe602cbd0b3/deploy-status)](https://app.netlify.com/sites/dazzling-mcclintock-d7ddec/deploys)

sustaphones is a site that lists android phones based on sustainability factors, like how hard it
is to replace the battery and whether a custom android rom supports it officially.

On the technical side, sustaphones is created by a custom static site generator written in ruby. It
consists of multiple scripts that together create the database, devices.yaml, and based on that
data create the html output.


## Setup and sync

Install the requirements:

```
sudo xbps-install ruby  # on void linux
gem install bundler
bundle install
npx -i purgecss
```

Update the data:

```
./update.sh
```

This can take a long while, as we have to query the online API for ifixit slowly.

Now check that the **devices.yaml** looks good. Specifically, if there are new devices you might
have to add the `headphone_jack` line. And depending on where the data comes from, also the `sdcard`
section might be missing. See the existing devices for orientation.

If all looks good, create the new site:

```
bundle exec ruby pagebuilder.rb
```

This will create the HTML and also call `npx purgecss` to create the minimized css, the only npm
requirement of this project (to be evaluated, it would be nicer to only rely on ruby).

Finally publish the changes to netlify:

```
git push
```

## What to do when there is a new ROM version

The main code that will need changing is in **pagebuilder.rb**. Extend the relevant
`translate_$ROM_version` function to map from the special version numbers of the custom roms to the
real android version, e.g. LineageOS 21 -> Android 14. The sort code below that function should
then work without further changes.

In the same file, if the new ROM release is based on an Android version that no other ROM is based
on yet, it is necessary to change the function `context.android_version_rating` that controls the
definition lists of the updated rom, so that outdated Android versions are marked accordingly.


## How to include a new ROM

 1. Write a new crawler/parser that adds the version information of the rom to devices.yaml
 1. Extend `translate_versions` in **pagebuilder.rb** to react to the new version, and the
 surrounding code that calls this function
 1. Add new layout code for the rom to **views/index.erb**, specifically the definition list for the
 phone's card and the dropdown menu in the filter section at the top.
 1. Add the new filter to the `valueNames` array in **public/html/js/main.js**.


## Concepts, nice to know

Devices get identified by their codename. By some magic the different projects are quite good in
using the same name for the same phone. The ifixit parser uses the phones' model names instead,
with some rules for translating them. Those need to be checked from time to time.

If a phone has multiple names (happens often for chinese phones) the `aka` list in **devices.yaml**
is the place for them. Do not add those phones multiple times under different codenames.

The site only lists phones and tablets, even though the database also knows of other devices like
set top boxes. There is a type filter.

The sort function for the site is not very clear, but works well. The battery exchangeability,
availability of a modern android version, the release year, whether it has a headphone jack and
whether it supports an sd card are the sort factors, in this order (currently).

For the sort order, a removable battery is best, it counts as much as when ifixit has a battery
repair guide that judges the process as easy or very easy. Note that some ifixit ratings are wrong,
phones with removable battery are then not marked as very easy, or the removal of a glued in
battery marked as easy (instead of moderate, the minimum for glued in bullshit). We have no process
yet to fix those errors, but could opt to just ignore devices that already have an ifixit rating
(and then correct errors manually in the .yaml).

ROM versions are colored by the Android version they are based on. The current policy (coded in
**pagebuilder.rb**) is to mark the current Android version green, the one before as well. The
Android version two steps behind is marked yellow, and everything older red. Goal is to communicate
to visitors that old versions will be less secure and not be supported for much longer.

Devices without active ROM options are not added to the page, but stay in the **devices.yaml**.
This reduces page load times and makes it easier to find actually valid device choices, while still
making it easy to bring devices back later.

Images are provided by ifixit. Because of the ifixit API data usage this site has to stay non
commercial - which is bad, it would be nice to show offers of phones, their current price would be
helpful.


## Possible development steps

 These are just some ideas:

 1. Replace the CSS framework bulma with something smaller, maybe custom CSS (but replacing the form
 styles could be hard). Initial approach here was to write as little CSS as possible, but that
 development challenge does not need to be continued.
 1. Improve the design, like working with custom colors instead of default bulma.
 1. Add more good ROMs.
 1. Add linux alternatives, when or if one gets useable.
 1. List the official vendor suport timerange, as an alternative to the custom roms.
 1. Add alternatives to ifixit repair tutorials, if there are any.